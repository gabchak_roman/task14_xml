<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<font face="courier">
					<th>
						<div style="background-color: #7d9456; color: #cdff78">
							<h1>Devices</h1>
						</div>
					</th>
				</font>
				<table border="1">
					<tr bgcolor="#cdff78">
						<th>Type</th>
						<th>Group</th>
						<th>Name</th>
						<th>Cooler</th>
						<th>Ports</th>
						<th>Energy_consumption</th>
						<th>Price</th>
						<th>Origin</th>
					</tr>
					<xsl:for-each select="devices/device">
						<tr>
							<td>
								<xsl:value-of select="type"/>
							</td>
							<td>
								<xsl:value-of select="group"/>
							</td>
							<td>
								<xsl:value-of select="name"/>
							</td>
							<td>
								<xsl:value-of select="cooler"/>
							</td>
							<td>
								<xsl:value-of select="ports"/>
							</td>
							<td>
								<xsl:value-of select="energy_consumption"/>
							</td>
							<td>
								<xsl:value-of select="price"/>
							</td>
							<td>
								<xsl:value-of select="origin"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>