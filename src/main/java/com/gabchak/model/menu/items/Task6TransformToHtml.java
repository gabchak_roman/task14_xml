package com.gabchak.model.menu.items;

import com.gabchak.model.task4.TransformXmlToHtml;
import com.gabchak.view.View;

import java.io.File;

import static java.lang.String.format;

public class Task6TransformToHtml extends AbstractMenuItem {
    private static final String NAME = "XSL transform tp HTML";
    private static final String DETAILS = "";

    public Task6TransformToHtml(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        String doc = new TransformXmlToHtml().transformToHtml(new File("computer.xml"));
        view.printMessage("Html doc: ");
        view.printYellowMessage(doc);
    }
}
