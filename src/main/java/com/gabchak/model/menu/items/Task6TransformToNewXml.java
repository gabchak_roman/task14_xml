package com.gabchak.model.menu.items;

import com.gabchak.model.task4.TransformXmlToXml;
import com.gabchak.view.View;

import java.io.File;

import static java.lang.String.format;

public class Task6TransformToNewXml extends AbstractMenuItem {
    private static final String NAME = "XSL transform to new XML";
    private static final String DETAILS = "(devices -> devices_changed)";

    public Task6TransformToNewXml(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        String doc = new TransformXmlToXml().transformToHtml(new File("computer.xml"));
        view.printMessage("New xml doc: ");
        view.printRedMessage(doc);
    }
}
