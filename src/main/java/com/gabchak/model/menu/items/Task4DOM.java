package com.gabchak.model.menu.items;

import com.gabchak.model.task4.ReadXmlWithDOM;
import com.gabchak.model.device.Device;
import com.gabchak.view.View;

import java.util.List;

import static java.lang.String.format;

public class Task4DOM extends AbstractMenuItem {
    private static final String NAME = "Parse with DOM";
    private static final String DETAILS = "";

    public Task4DOM(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        view.printRedMessage("Parsed with DOM");
        List<Device> devices = new ReadXmlWithDOM().readXml();
        view.print(devices);
    }
}
