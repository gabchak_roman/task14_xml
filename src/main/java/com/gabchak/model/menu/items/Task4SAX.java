package com.gabchak.model.menu.items;

import com.gabchak.model.task4.ReadXmlWithSAX;
import com.gabchak.model.device.Device;
import com.gabchak.view.View;

import java.util.List;

import static java.lang.String.format;

public class Task4SAX extends AbstractMenuItem {
    private static final String NAME = "Parse with SAX";
    private static final String DETAILS = "";

    public Task4SAX(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        view.printGreenMessage("Parsed with SAX");
        List<Device> devices = new ReadXmlWithSAX().readXml();
        view.print(devices);
    }
}
