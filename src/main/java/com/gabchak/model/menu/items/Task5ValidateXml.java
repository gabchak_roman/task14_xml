package com.gabchak.model.menu.items;

import com.gabchak.model.task4.CheckXmlWithXsd;
import com.gabchak.view.View;

import java.io.File;

import static java.lang.String.format;

public class Task5ValidateXml extends AbstractMenuItem {
    private static final String NAME = "Validate with DOM";
    private static final String DETAILS = "";

    public Task5ValidateXml(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {

        boolean isValid = new CheckXmlWithXsd(view).validateXml(
       new File("computer_invalid.xml"),
               new File("computers.XSD"));

        view.printMessage("Is valid: " + isValid);
    }
}
