package com.gabchak.model.menu.items;

import com.gabchak.model.MenuItem;
import com.gabchak.view.View;

public abstract class AbstractMenuItem implements MenuItem {
    static final String TITLE_FORMAT = "%-10s - %-25s %s";

    private String title;
    private String key;
    protected View view;

    protected AbstractMenuItem(String key, String title, View view) {
        this.key = key;
        this.title = title;
        this.view = view;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public abstract void execute();
}
