package com.gabchak.model.menu.items;

import com.gabchak.model.task4.ReadXmlWithSAX;
import com.gabchak.model.device.Device;
import com.gabchak.view.View;

import java.util.List;

import static java.lang.String.format;

public class Task4StAX extends AbstractMenuItem {
    private static final String NAME = "Parse with StAX";
    private static final String DETAILS = "";

    public Task4StAX(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        view.printYellowMessage("Parsed with StAX");
        List<Device> devices = new ReadXmlWithSAX().readXml();
        view.print(devices);
    }
}
