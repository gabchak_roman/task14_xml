package com.gabchak.model.menu.items;

import com.gabchak.view.View;

public class DummyItem extends AbstractMenuItem {
    private final String message;

    public DummyItem(View view, String message) {
        super(null, null, view);
        this.message = message;
    }

    @Override
    public void execute() {
        view.printMessage(message);
    }
}
