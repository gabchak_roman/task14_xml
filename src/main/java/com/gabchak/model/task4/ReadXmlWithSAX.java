package com.gabchak.model.task4;

import com.gabchak.model.device.Device;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class ReadXmlWithSAX {

    public static void main(String[] args) {
        List<Device> devices = new ReadXmlWithSAX().readXml();
        devices.forEach(System.out::println);
    }

    public List<Device> readXml() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler handler = new SaxHandler();
            saxParser.parse(new File("computer.xml"), handler);

            return handler.getDevices();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println("Error parsing xml. " + e.getMessage());
        }
        return null;
    }
}
