package com.gabchak.model.task4;

import com.gabchak.model.Devices;
import com.gabchak.view.View;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class CheckXmlWithXsd {

    private View view;

    public CheckXmlWithXsd(View view) {
        this.view = view;
    }

    private boolean isValid = true;

    public boolean validateXml(File xmlFile, File xsdSchemaFile) {
        try {
            isValid = true;

            JAXBContext context = JAXBContext.newInstance(Devices.class);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(xsdSchemaFile);

            Unmarshaller unmarshaller = context.createUnmarshaller();

            unmarshaller.setSchema(schema);
            unmarshaller.setEventHandler(new EmployeeValidationEventHandler());

            unmarshaller.unmarshal(xmlFile);

            return isValid;
        } catch (JAXBException | SAXException e) {
            System.out.println("Error validating xml. " + e.getMessage());
            return false;
        }
    }

    class EmployeeValidationEventHandler implements ValidationEventHandler {
        @Override
        public boolean handleEvent(ValidationEvent event) {
            view.printRedMessage("\nError:");
            view.printRedMessage(event.getMessage());
            view.printRedMessage("    line:  " + event.getLocator().getLineNumber());
            view.printRedMessage("    column:  " + event.getLocator().getColumnNumber());

            isValid = false;
            return true;
        }
    }
}
