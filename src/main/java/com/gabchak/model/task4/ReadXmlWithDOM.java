package com.gabchak.model.task4;

import com.gabchak.model.device.Device;
import com.gabchak.model.Devices;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class ReadXmlWithDOM {

    public static void main(String[] args) {
        List<Device> devices = new ReadXmlWithDOM().readXml();
        devices.forEach(System.out::println);
    }

    public List<Device> readXml() {
        try {
            File devicesXmlFile = new File("computer.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Devices.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Devices devices = (Devices) jaxbUnmarshaller.unmarshal(devicesXmlFile);
            return devices.getDevice();
        } catch (JAXBException e) {
            System.out.println("Error parsing xml. " + e.getMessage());
        }
        return null;
    }
}
