package com.gabchak.model.task4;

import com.gabchak.model.device.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SaxHandler  extends DefaultHandler {

    private List<Device> devices = new ArrayList<>();
    private Device device = null;
    private StringBuilder data = null;

    public List<Device> getDevices() {
        return devices;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        if (qName.equalsIgnoreCase("device")) {
            boolean critical = Boolean.parseBoolean(attributes.getValue("critical"));
            device = new Device();
            device.setCritical(critical);
        } else if (qName.equalsIgnoreCase("ports")) {
            device.setPorts(new PortsElement());
        }
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("name")) {
            device.setName(data.toString());
        } else if (qName.equalsIgnoreCase("price")) {
            device.setPrice(new BigDecimal(data.toString()));
        } else if (qName.equalsIgnoreCase("origin")) {
            device.setOrigin(data.toString());
        } else if (qName.equalsIgnoreCase("type")) {
            device.setType(DeviceType.valueOf(data.toString()));
        } else if (qName.equalsIgnoreCase("group")) {
            device.setGroup(DeviceGroup.valueOf(data.toString()));
        } else if (qName.equalsIgnoreCase("energy_consumption")) {
            device.setEnergyConsumption(Integer.valueOf(data.toString()));
        } else if (qName.equalsIgnoreCase("cooler")) {
            device.setHasCooler(Boolean.parseBoolean(data.toString()));
        } else if (qName.equalsIgnoreCase("port")) {
            device.addPort(Port.valueOf(data.toString()));
        }

        // clear
        data.setLength(0);

        if (qName.equalsIgnoreCase("device")) {
            devices.add(device);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        data.append(new String(ch, start, length));
    }
}
