package com.gabchak.model.task4;

import java.io.File;

public class TransformXmlToXml extends BaseXmlTransformer{

    public String transformToHtml(File xmlFile) {
        File xmlToHtmlXsl = new File("computer_new_xml.xsl");
        return transformToHtml(xmlFile, xmlToHtmlXsl);
    }
}
