package com.gabchak.model.task4;

import java.io.File;

public class TransformXmlToHtml extends BaseXmlTransformer{

    public String transformToHtml(File xmlFile) {
        File xmlToHtmlXsl = new File("computer_html.xsl");
        return transformToHtml(xmlFile, xmlToHtmlXsl);
    }
}
