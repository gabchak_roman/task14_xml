package com.gabchak.model.task4;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

class BaseXmlTransformer {
    String transformToHtml(File xmlFile, File xsl) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFile);

            // Use a Transformer for output
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            StreamSource style = new StreamSource(xsl);
            Transformer transformer = transformerFactory.newTransformer(style);

            DOMSource source = new DOMSource(document);
            StringWriter stringWriter = new StringWriter();
            StreamResult result = new StreamResult(stringWriter);
            transformer.transform(source, result);
            return stringWriter.toString();
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            System.out.println("Error parsing the config. " + e.getMessage());
        }
        return null;
    }
}
