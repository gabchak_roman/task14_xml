package com.gabchak.model.task4;

import com.gabchak.model.device.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ReadXmlWithStAX {

    public static void main(String[] args) {
        List<Device> devices = new ReadXmlWithStAX().readXml();
        assert devices != null;
        devices.forEach(System.out::println);
    }

    private List<Device> readXml() {
        try {
            return parseDevices(new File("computer.xml"));
        } catch (IOException | XMLStreamException e) {
            System.out.println("Error parsing xml. " + e.getMessage());
        }
        return null;
    }

    private List<Device> parseDevices(File file) throws FileNotFoundException, XMLStreamException {
        List<Device> devices = new ArrayList<>();

        Device device = new Device();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String data = xmlEvent.asCharacters().getData();
                switch (startElement.getName().getLocalPart()) {
                    case "device":
                        device = new Device();
                        setCriticalValue(device, startElement);
                        break;
                    case "name":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setName(data);
                        break;
                    case "price":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setPrice(new BigDecimal(data));
                        break;
                    case "origin":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setOrigin(data);
                        break;
                    case "type":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setType(DeviceType.valueOf(data));
                        break;
                    case "group":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setGroup(DeviceGroup.valueOf(data));
                        break;
                    case "energy_consumption":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setEnergyConsumption(Integer.valueOf(data));
                        break;
                    case "cooler":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.setHasCooler(Boolean.parseBoolean(data));
                        break;
                    case "port":
                        xmlEvent = xmlEventReader.nextEvent();
                        device.addPort(Port.valueOf(data));
                        break;
                    case "ports":
                        device.setPorts(new PortsElement());
                        break;
                }
            }
            //if Employee end element is reached, add employee object to list
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("device")) {
                    devices.add(device);
                    device = new Device();
                }
            }
        }
        return devices;
    }

    private void setCriticalValue(Device device, StartElement startElement) {
        Attribute criticalAttr = startElement.getAttributeByName(
                new QName("critical"));
        if (criticalAttr != null) {
            device.setCritical(Boolean.parseBoolean(criticalAttr.getValue()));
        }
    }
}
