package com.gabchak.model.device;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.HashSet;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
public class PortsElement {
    @XmlElement(name = "port")
    private Set<Port> ports = new HashSet<>();

    public Set<Port> getPorts() {
        return ports;
    }

    public void setPorts(Set<Port> ports) {
        this.ports = ports;
    }

    @Override
    public String toString() {
        return String.valueOf(ports);
    }
}
