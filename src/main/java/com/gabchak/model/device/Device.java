package com.gabchak.model.device;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class Device {

    private String name;
    private DeviceType type;
    private DeviceGroup group;

    @XmlAttribute(name="critical")
    private boolean isCritical;

    @XmlElement(name="cooler")
    private boolean hasCooler;

    @XmlElement(name="energy_consumption")
    private Integer energyConsumption;

    private BigDecimal price;
    private String origin;
    private PortsElement ports;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public DeviceGroup getGroup() {
        return group;
    }

    public void setGroup(DeviceGroup group) {
        this.group = group;
    }

    public boolean isCritical() {
        return isCritical;
    }

    public void setCritical(boolean critical) {
        isCritical = critical;
    }

    public boolean hasCooler() {
        return hasCooler;
    }

    public void setHasCooler(boolean hasCooler) {
        this.hasCooler = hasCooler;
    }

    public Integer getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(Integer energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public PortsElement getPorts() {
        return ports;
    }

    public void addPort(Port port) {
        if (ports == null) {
            ports = new PortsElement();
        }
        ports.getPorts().add(port);
    }

    public void setPorts(PortsElement ports) {
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", group=" + group +
                ", isCritical=" + isCritical +
                ", hasCooler=" + hasCooler +
                ", energyConsumption=" + energyConsumption +
                ", price=" + price +
                ", origin='" + origin + '\'' +
                ", ports=" + ports +
                '}';
    }
}
