package com.gabchak.model.device;

public enum Port {
    COM, USB, LPT, HDMI, VGA, DVI, USB_TYPE_C;
}
