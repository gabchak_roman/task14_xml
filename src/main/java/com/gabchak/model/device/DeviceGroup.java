package com.gabchak.model.device;

public enum DeviceGroup {
    CPU, MOTHERBOARD, RAM, GPU, POWER_BLOCK, KEYBOARD, MONITOR, MOUSE;
}
