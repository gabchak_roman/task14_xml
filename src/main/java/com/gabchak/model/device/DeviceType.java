package com.gabchak.model.device;

public enum DeviceType {
    CORE, PERIPHERAL;
}
