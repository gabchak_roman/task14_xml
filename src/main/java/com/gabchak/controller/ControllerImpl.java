package com.gabchak.controller;

import com.gabchak.model.MenuItem;
import com.gabchak.model.menu.items.*;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Map<String, MenuItem> menu;
    private MenuItem notExistingItem;

    public ControllerImpl(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
    }

    public void start() {
        buildMenu();
        startMainLoop();
    }

    private void buildMenu() {
        menu = new LinkedHashMap<>();

        List<MenuItem> items = new ArrayList<>();
        items.add(new Task4DOM("4", "Task 4.1", view));
        items.add(new Task4SAX("5", "Task 4.2", view));
        items.add(new Task4StAX("6", "Task 4.3", view));
        items.add(new Task5ValidateXml("7", "Task 5", view));
        items.add(new Task6TransformToHtml("8", "Task 6.1", view));
        items.add(new Task6TransformToNewXml("9", "Task 6.2", view));
        items.add(new AbstractMenuItem("Q", "Exit", view) {
            @Override
            public void execute() {
                view.printMessage("Bye");
                System.exit(0);
            }
        });

        notExistingItem = new DummyItem(view, "No such menu item.");

        items.forEach(i -> menu.put(i.getKey(), i));
    }

    private void startMainLoop() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select buildMenu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            menu.getOrDefault(keyMenu, notExistingItem).execute();
        } while (true);
    }

    private void outputMenu() {
        view.printMessage("\nMENU:");
        menu.values().forEach(item -> view.printMessage(
                format(" %s - %s", item.getKey(), item.getTitle())));
    }
}