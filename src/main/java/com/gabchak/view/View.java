package com.gabchak.view;

import com.gabchak.model.device.Device;

import java.util.List;

public interface View {

    void printMessage(String message);

    void print(List<Device> devices);

    void printYellowMessage(String message);

    void printGreenMessage(String message);

    void printRedMessage(String message);
}
