<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<devices_changed>
			<xsl:for-each select="devices/device">
				<device>
					<xsl:attribute name="critical">
						<xsl:value-of select="@critical" />
					</xsl:attribute>
					<type><xsl:value-of select="type"/></type>
					<group><xsl:value-of select="group"/></group>
					<name><xsl:value-of select="name"/></name>
					<price><xsl:value-of select="price"/></price>
					<origin><xsl:value-of select="origin"/></origin>
					<energy_consumption><xsl:value-of select="energy_consumption"/></energy_consumption>
					<cooler><xsl:value-of select="cooler"/></cooler>
					<xsl:if test="ports">
						<ports>
							<xsl:for-each select="ports/port">
								<port><xsl:value-of select="."/></port>
							</xsl:for-each>
						</ports>
					</xsl:if>
				</device>
			</xsl:for-each>
		</devices_changed>
	</xsl:template>
</xsl:stylesheet>